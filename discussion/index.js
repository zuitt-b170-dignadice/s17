/* 

How do we display the following tasks in the conosole?

    1. Drink HTM:
    2. Eat javascript
    3. Inhale css
    4/ bake bootstrap

*/

/* 

arrays - are used to store multiple related data values inside a single variable. Arrays are declared using the square brackets ( [])

SYNTAX 
    let/const<array name> = ['elementA','elementB'...]

    arrays are used if there is a need to manuipulate the related vbalues stored in it.

    index - position of each element in the array. Calling an element through the index id determined by using this syntax : 
        arrayName[index]
        Reminder : index is always starting with 0. 
        Counting the elements inside would start with 0 instead of 1

        formula: nth element -1/ arrayName.length - 1
            -start counting with 0

*/

console.log('drink html');
console.log('eat javascript');
console.log('inhale css');
console.log('bake bootstrap');

let tasks = ['drink html', 'eat javascript', 'inhale css', 'bake bootstrap']
console.log(tasks);
// getting the element through the array's index
console.log(tasks[2]);




/*

let indexOfLastElement = tasks.length -1;
console.log(indexOfLastElement); 

*/

// getting the number of elements
console.log(tasks.length);

// trying to access the index of a non-existing element - would result to undefined
console.log(tasks[4]);

/* ================================ */

// Array Manipulation
// ADDING an element

let numbers = ['one', 'two', 'three', 'four'];
console.log(numbers);
console.log(numbers[4]);

// using assignment operator
numbers[4] = 'five';
console.log(numbers);

// push method
numbers.push('element');
console.log(numbers);

// callback function - a function that is passed onto another function. this is done because the inserted function is following a particualr syntax and the developer is trying to simplify that syntax by just inserting it inside another function.

function pushMethod(element){
    numbers.push(element);
}

pushMethod('six');
pushMethod('seven');
pushMethod('eight');
console.log(numbers);

// Removing of an Element

// pop method - removes the element at the end of the array (last element)
numbers.pop();
console.log(numbers);

function popMethod(){
    numbers.pop();
}

popMethod();
/* popMethod();
popMethod();
popMethod();
popMethod();
popMethod();
popMethod(); */
console.log(numbers);

// ================================

// Manipulating the beginning/start of the array
// remove the first element
// shift method

numbers.shift();
console.log(numbers);

// callback funtion
function shiftMethod(){
    numbers.shift();
}

shiftMethod();
console.log(numbers);

//============================
// Adding an Element
// unshift

numbers.unshift('zero');
console.log(numbers);

function unshiftMethod(developer){ 

    // unshiftMethod(declare name of variable)
    numbers.unshift(developer);
}

unshiftMethod('mcdo');
unshiftMethod('jollibee');
unshiftMethod('1');
console.log(numbers);

// Arrangement of the elements

let numbs = [15,25,32,12,6,8,236]
// sort method - arranges the elements in ascending or descending. it has an anonymous function isnide that has 2 parameters.

    // anonymous function - unnabmed fuction and can only be used once.

    /* 
    
        2 parameters inside the anonymous fuction represents : 
            first parameter - first/smallest/starting element
            second parameter - last/biggest/ending element
            

    */
console.log(numbs);
// Ascending
/* 

    Synttax :
        arrayName.sort(
            function(a,b){
                return a - b ascending
                return b - a descending
            }
        )

*/
numbs.sort(
    function(a,b){
        return a -b
    }
)

console.log(numbs)

// Descending

numbs.sort(
    function(a,b){
        return b-a
    }
)

console.log(numbs);

// reverse method - reverses the order of the elements in an array, regardless if it isi ascending, decending have in random order

numbs.reverse();
console.log(numbs);

//===========================================
// Splice method - ctrl x + ctrl p

/* 

    - directly manipulates the array
    - first parameter, the index of the element from which the omiotting will begin
    - second parameter - determines the number of elements to be omitted
    - third parameter onwards - replace the elements that is ommitted

    SYNTAX 

        let/const <newArray> = <originalArray>.splice(firstParam, secondParam, thirdParam,....)

*/
// one parameter (pure omission)
// let nums = numbs.splice(1);

// two parameters
// let nums = numbs.splice(1,2);

// three parameters : replacements
let nums = numbs.splice(4,2,31,11, 111);
console.log(numbs);
console.log(nums);


// =============================
// slice method - ctrl c + ctrl p
/* 

    - does not affect the original array; creates a sub array but does not omit any element from the original array.
    - first parameter - index where copying will begin
    - photocopy method
    - second parameter - the number of elements to be copied starting from the frist element (copying begins from the first parameter )
    
     SYNTAX 

        let/const <newArray> = <originalArray>.slice(firstParam, secondParam)
    

*/

// one parameter
// let slicedNums = numbs.slice(1);
let slicedNums = numbs.slice(2,7); // (x_index, y_elemen)
console.log(numbs);
console.log(slicedNums);


//=============================
// Merging of array

// Concat

console.log(numbers);
console.log(numbs);
let animals = ['dog', 'tiger', 'kangaroo', 'chimken'];
console.log(animals);

let newConcat = numbers.concat(numbs, animals);
console.log(newConcat);
console.log(numbers);
console.log(numbs);
console.log(animals);

// join method - merges the elemets and makes them string data


let meal = ['rice', 'steak', 'juice'];

let newJoin = meal.join(""); // ricesteakjuice

newJoin = meal.join(" "); // rice steak juice
console.log(newJoin);

newJoin = meal.join("-"); // rice-steak-juice
console.log(newJoin);

// toString method - converts the element into string data type
console.log(nums);
// typeof - determines the data type of the element after it.
console.log(typeof nums);

let newString = nums.toString();
console.log(typeof newString)
console.log(newString);

// Accessors

let countries = ['US', 'PH', 'JPN', 'HK', 'SG', 'PH', 'NZ']
// indexOf - the first index it finds from the beginning of the array.
let index = countries.indexOf('PH')
console.log(index);
// finding a  non-existing element > returns -1
index = countries.indexOf('AU')
console.log(index);

// lastIndexOF()
index = countries.lastIndexOf("PH");
console.log(index);

// returns -1 for non-existing elements
index = countries.lastIndexOf("h");
console.log(index);

/* if (countries.indexOf("CAN") === -1) {
	console.log("Element not existing");
}else{
	console.log("Element exists in the array");
}
 */
// Iterators 


let days = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun']

// for each - returns (performs the statement in) each element in the array
/* 
array.forEach(
    function(element){
        statement/s
    }
)

*/
days.forEach(
    function(element){
        console.log(element)
    }
)

// map - returns a copy of an array from the original which can be manipulated 

/* 

array.map(
    function.element{

    }
)

*/

let mapDays = days.map(
    function(element){
        return `${element} is the day of the week`
    }
)

console.log(mapDays);
console.log(days);

// filter - filters the elements and copies them into another array
console.log(numbs);
let newFilter = numbs.filter(
    function(element){
        return element < 30
    }
)

console.log(newFilter);

// includes - return true (bool) if the element/s are inside the array.
let animalIncludes = animals.includes('dog');
console.log(animalIncludes);

// every - checkes if all the element pass the condition (returns true if all of them pass)
let newEvery = nums.every(
    function (element){
        return (element > 30);
    }
)

console.log(newEvery);

// some - checks if at least  1 elemebt passes the condition

let newSome = nums.some(
    function(element){
        return(element > 30)
    }
)

console.log(newSome);

nums.push(50)
console.log(nums);
// reduce - performs the operation in all of the elemets in the array
let newReduce = nums.reduce(
    function(a, b){
        return a + b 
        // return b - a
    }
)
console.log(newReduce);

let average = newReduce/nums.length
console.log(average);

// toFixed
console.log(average.toFixed(2));

/* 
parseInt  -rounds the number to the nearest whole number
parseFload - uinclued the decimal place, sets limit by to fixed

*/

console.log(parseInt(average.toFixed(2)));
console.log(parseFloat(average.toFixed(2)));






























